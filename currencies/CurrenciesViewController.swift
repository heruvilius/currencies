//
//  ViewController.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import UIKit
import Moya

class CurrenciesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editTableButton: UIButton!
    @IBOutlet weak var noConnectionLabel: UILabel!
    
    private var currencies = [CurrencyModel]()
    private var presenter = CurrenciesPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureText()
        configurePullToRefresh()
        presenter.attachView(self)
        presenter.getCurrencies()        
    }
    
    private func configureText() {
        titleLabel.text = "Курсы валют"
        editTableButton.setTitle("Изменить", for: .normal)
        noConnectionLabel.text = "Нет соединения..."
    }
    
    @IBAction func editTableButtonPressed() {
        tableView.isEditing = !tableView.isEditing
    }
    
}

//  MARK: - UITableViewDataSource
extension CurrenciesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.ReuseIdentifier, for: indexPath) as! CurrencyCell
        cell.configure(with: currencies[indexPath.row])
        return cell
    }
 
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let currencyToBeMoved = currencies[sourceIndexPath.row]
        currencies.remove(at: sourceIndexPath.row)
        currencies.insert(currencyToBeMoved, at: destinationIndexPath.row)
    }
    
}

//  MARK: - CurrenciesView
extension CurrenciesViewController: CurrenciesView {
    
    func startLoading() {
        noConnectionLabel.isHidden = true
    }
    
    func finishLoading() {
        tableView.refreshControl?.endRefreshing()
    }
    
    func presentError(_ error: NSError) {
        let alert = UIAlertController(title: "Ошибка \(error.code)",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    func update(with currencies: [CurrencyModel]) {
        self.currencies = currencies
        tableView.reloadData()
    }
    
    func presentConnectionError() {
        noConnectionLabel.isHidden = false
        currencies = [CurrencyModel]()
        tableView.reloadData()
        tableView.refreshControl?.endRefreshing()
    }
    
}

//  MARK: - Pull-To-Refresh
extension CurrenciesViewController {
    
    func configurePullToRefresh() {
        if tableView.refreshControl == nil {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self,
                                     action: #selector(refreshControlValueChanged),
                                     for: .valueChanged)
            refreshControl.tintColor = .black
            tableView.refreshControl = refreshControl
        }
    }
    
    @objc func refreshControlValueChanged() {
        //  Disabling gesture recognizer stops scrolling
        for gestureRecognizer in tableView.gestureRecognizers! {
            let enabled = gestureRecognizer.isEnabled
            gestureRecognizer.isEnabled = false
            gestureRecognizer.isEnabled = enabled
        }
        presenter.refreshCurrencies()
    }
    
}

