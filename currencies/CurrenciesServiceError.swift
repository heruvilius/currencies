//
//  CurrenciesServiceError.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation

enum CurrenciesServiceError: Error {
    case noConnection
    case serverError(code: Int)
    case cantParseXML
    case unknown
}
