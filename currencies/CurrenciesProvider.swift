//
//  CurrenciesProvider.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation
import Moya

struct CurrenciesProvider {
    
    private init() { }

    private static let provider = MoyaProvider<CurrenciesAPI>()
    
    fileprivate typealias `Self` = CurrenciesProvider

    static func request(_ target: CurrenciesAPI, completion: @escaping (Response?, NSError?) -> Void) -> Cancellable {
        let token = Self.provider.request(target) { (result) in
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                let nsError = error as NSError?
                //  Code -999 = `cancel`
                if nsError?.code != -999 {
                    completion(nil, nsError)
                }
            }
        }
        return token
    }
    
}
