//
//  CurrencyModel.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation

class CurrencyModel: NSObject {
    
    private(set) var charCode: String
    private(set) var scale: Int
    private(set) var name: String
    private(set) var rate: Float

    init(charCode: String, scale: Int, name: String, rate: Float) {
        self.charCode = charCode
        self.scale = scale
        self.name = name
        self.rate = rate
        super.init()
    }
    
    init(string: String) throws {
        charCode = try CurrencyModel.value(from: string, key: "CharCode")
        scale = try Int(CurrencyModel.value(from: string, key: "Scale"))!
        name = try CurrencyModel.value(from: string, key: "Name")
        rate = try Float(CurrencyModel.value(from: string, key: "Rate"))!
        super.init()
    }
    
    static func value(from string: String, key: String) throws -> String {
        let components = string.components(separatedBy: key)
        if components.count < 2 {
            throw NSError(domain: "XMLParsingError", code: 1, userInfo: [NSLocalizedDescriptionKey: "Неверный формат ответа."])
        }
        var value = components[1]
        value.removeFirst()
        value.removeLast(2)
        return value
    }
    
}
