//
//  CurrenciesService.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation
import Moya

struct CurrenciesService {
    
    private init() { }

    fileprivate typealias `Self` = CurrenciesService
    
    static func requestCurrencies(completion: @escaping ([CurrencyModel]?, NSError?) -> Void) -> Cancellable {
        let token = CurrenciesProvider.request(.currencies) {
            (requestResponse, requestError) in
            if requestError != nil {
                completion(nil, requestError)
            } else if requestResponse != nil {
                do {
                    let currencies = try deserializeXMLResponse(requestResponse!)
                    completion(currencies, nil)
                } catch let error as NSError {
                    completion(nil, error)
                }
            } else {
                completion(nil, NSError.init(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "Ошибка получения ответа от сервера"]))
            }
        }
        return token
    }
    
    private static func deserializeXMLResponse(_ response: Response) throws -> [CurrencyModel] {
        let xmlString = try response.mapString()
        let components = xmlString.components(separatedBy: "<Currency")
        var currencies = [CurrencyModel]()
        for i in 1 ..< components.count {
            let currency = try CurrencyModel(string: components[i])
            currencies.append(currency)
        }
        return currencies
    }
    
}
