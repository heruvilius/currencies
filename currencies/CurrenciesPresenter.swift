//
//  CurrenciesPresenter.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation
import Moya
import Alamofire

protocol CurrenciesView: NSObjectProtocol {
    
    func startLoading()
    func finishLoading()
    func update(with currencies: [CurrencyModel])
    func presentError(_ error: NSError)
    func presentConnectionError()
    
}

class CurrenciesPresenter: NSObject {
    
    weak var view: CurrenciesView?
    private var token: Cancellable?
    
    func attachView(_ view: CurrenciesView){
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getCurrencies(){
        view?.startLoading()
        token?.cancel()
        if !isCurrenciesServerAvailable() {
            view?.presentConnectionError()
            return
        }
        token = CurrenciesService.requestCurrencies {
            [weak self] (currencies, error) in
            self?.view?.finishLoading()
            if error != nil {
                self?.view?.presentError(error!)
            } else {
                self?.view?.update(with: currencies!)
            }
        }
    }
    
    private func isCurrenciesServerAvailable() -> Bool {
        let manager = NetworkReachabilityManager(host: CurrenciesAPI.currencies.baseURL.absoluteString)
        return manager?.networkReachabilityStatus != .notReachable
    }
    
    func refreshCurrencies() {
        URLCache.shared.removeAllCachedResponses()
        getCurrencies()
    }
    
}

