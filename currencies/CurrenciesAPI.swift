import Foundation
import Moya

//  MARK: - API
/// A list of supported requests
enum CurrenciesAPI {
    
    ///
    case currencies
    
}

//  MARK: - MOYA Target Type
extension CurrenciesAPI: TargetType {
    
    /// Address for requests
    var baseURL: URL { return URL(string: "http://www.nbrb.by")! }
    
    ///  Extra path to make full route for Server call
    var path: String { return "/Services/XmlExRates.aspx" }
    
    /// Method type for a particular request path
    var method: Moya.Method { return .get }
    
    /// Data to be returned if the internet connection is down
    var sampleData: Data {
        return Data()
    }
    
    ///  Task type for particular request path
    var task: Task {
        return Task.requestPlain
    }
    
    /// HTTP Headers
    var headers: [String : String]? { return nil }

}
