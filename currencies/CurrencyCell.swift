//
//  CurrencyCell.swift
//  currencies
//
//  Created by Heruvilius on 8/19/18.
//  Copyright © 2018 heruvilius. All rights reserved.
//

import Foundation
import UIKit

class CurrencyCell: UITableViewCell {
    
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var scaleLabel: UILabel!

    static let ReuseIdentifier = "CurrencyCell"

    func configure(with currency: CurrencyModel) {
        rateLabel.text = String(format: "%@ %.4f BYN", currency.charCode, currency.rate)
        scaleLabel.text = String(format: "%@ за %d ед.", currency.name, currency.scale)
    }
    
}
